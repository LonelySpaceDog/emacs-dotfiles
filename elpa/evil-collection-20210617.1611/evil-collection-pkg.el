(define-package "evil-collection" "20210617.1611" "A set of keybindings for Evil mode"
  '((emacs "25.1")
    (evil "1.2.13")
    (annalist "1.0"))
  :commit "2849c80c79e730454410bf259981e4d31eeef9b2" :authors
  '(("James Nguyen" . "james@jojojames.com"))
  :maintainer
  '("James Nguyen" . "james@jojojames.com")
  :keywords
  '("evil" "tools")
  :url "https://github.com/emacs-evil/evil-collection")
;; Local Variables:
;; no-byte-compile: t
;; End:
