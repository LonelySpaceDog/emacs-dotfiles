(define-package "doom-modeline" "20210618.2057" "A minimal and modern mode-line"
  '((emacs "25.1")
    (all-the-icons "2.2.0")
    (shrink-path "0.2.0")
    (dash "2.11.0"))
  :commit "a9186b7cee406199c76dea23b75a66a9c3235015" :authors
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("faces" "mode-line")
  :url "https://github.com/seagle0128/doom-modeline")
;; Local Variables:
;; no-byte-compile: t
;; End:
