(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("716f0a8a9370912d9e6659948c2cb139c164b57ef5fda0f337f0f77d47fe9073" default))
 '(package-selected-packages
   '(melancholy-theme company quelpa term-cursor flycheck rustic rustic-mode visual-fill-column org-bullets forge evil-magit magit counsel-projectile projectile smartparens smartparens-config smarptarens-config smarptarens smarparens-config smarparens hydra evil-collection evil general doom-themes helpful counsel ivy-rich which-key rainbow-delimiters use-package ivy doom-modeline command-log-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
