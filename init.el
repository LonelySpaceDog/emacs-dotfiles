
(setq inhibit-startup-message t)

(scroll-bar-mode -1) ;Disable visible scrollbar
(tool-bar-mode -1)   ;Disable the toolbar
(tooltip-mode -1)    ;Disable tooltips
(set-fringe-mode 10) ;Give some breathing room
(menu-bar-mode -1) ; Disable the menu bar

;;Set Visible bell
(setq visible-bell nil)

;; Make ESC quit promts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;;Font
(set-face-attribute 'default nil :font "RobotoMono Nerd Font" :height 120)

;;Theme
;(load-theme 'tango-dark t) ;Dark build in theme

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'quelpa)
  (with-temp-buffer
    (url-insert-file-contents "https://raw.githubusercontent.com/quelpa/quelpa/master/quelpa.el")
    (eval-buffer)
        (quelpa-self-upgrade)))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; Column number mode
(column-number-mode)
(global-display-line-numbers-mode t)

;; Disable column numbers for some modes
(dolist (mode '(org-mode-hook
		term-mode-hook
		shell-mode-hook
		eshell-mode-hool))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;;Rainbow mode
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(require 'use-package)
(setq use-package-always-ensure t)

;;(use-package command-log-mode)

;;Ivy things
(use-package swiper)
(use-package ivy
  :diminish
  :bind (("M-s" . swiper)
	 :map ivy-minibuffer-map
	 ("TAB" . ivy-alt-done)
	 ("M-l" . ivy-alt-done)
	 ("M-j" . ivy-next-line)
	 ("M-k" . ivy-previous-line)
	 :map ivy-switch-buffer-map
	 ("M-k" . ivy-previous-line)
	 ("M-l" . ivy-done)
	 ("M-d" . ivy-switch-buffer-kill)
	 :map ivy-reverse-i-search-map
	 ("M-k" . ivy-previous-line)
	 ("M-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
	  ("C-x b" . counsel-ibuffer)
	  ("C-x C-f" . counsel-find-file)
	  :map minibuffer-local-map
	  ("C-r" . 'counsel-minibuffer-history)))
;;:config
;;  (setq ivy-initial-inputs-alist nil)) ;;Don't start searches with ^

(use-package swiper)  
(use-package ivy-rich
  :init (ivy-rich-mode 1))

;;Nice modeline and themes
(use-package all-the-icons)

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

(use-package doom-themes)
;  :init (unless (display-graphic-p)
 ; (load-theme 'doom-1337 t)
 ; ))

(use-package melancholy-theme
  :init (load-theme 'melancholy))

(add-hook 'window-setup-hook 'on-after-init)

;;Help for keybindigs
(use-package which-key
  :init (which-key-mode 1)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.6))

;;Helpfull thing
(use-package helpful
  :ensure t
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package general
  :config
  (general-create-definer rune/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC") 

  (rune/leader-keys
    ;;togles
    "t" '(:ignore t :which-key "toggles")
    "ts" '(hydra-text-scale/body :which-key "Scale text")
    ;;buffers
    "b" '(:ignore t :which-key "buffers")
    "bb" '(counsel-switch-buffer :which-key "switch buffer")
    "bn" '(switch-to-next-buffer :which-key "next buffer")
    "bp" '(switch-to-previous-buffer :which-key "previous buffer")
    "bk" '(kill-buffer :which-key "Kill buffer")
    ;;files
    "f" '(:ignore t :which-key "files")
    "ff" '(find-file :which-key "find-file")
    ;;evil window
    "w" '(evil-window-map :which-key "windows")
    ;;Emacs keys
    "e" '(:ignore t :which-key "emacs")
    "ee" '(eval-last-sexp :which-key "eval-last-sexp")
    ":" '(counsel-M-x :which-key "M-x")
    ;;Git and projectile
    "g" '(:ignore t :which-key "Git and Projectile")
    "gp" '(projectile-command-map :which-key "Projectile")
    "gs" '(magit-status :which-key "magit-status")
    ;;Smartparens
    "p" '(:ignore t :which-key "smartparens")
    "pr" '(sp-wrap-round :which-key "wrap with round parens")
    "pc" '(sp-wrap-cancel :which-key "wrap cancel")
    "pt" '(sp-wrap-square :which-key "wrap with square parnes")
    "pe" '(sp-wrap-curly :which-key "wrap with curly parnes")
    ;;Org mode
    "o" '(:ignore t :which-key "Org mode stuff")
    ;;Lsp
 ;   "l" '(lsp-command-map :which-key "LSP")
    ;;Rust
    "m" '(:ignore t :which-key "Rust")
    "mu" '(lsp-ui-imenu :which-key "lsp-ui-imenu")
    "mf" '(lsp-find-references :which-key "lsp-find-references")
    "mba" '(lsp-execute-code-action :which-key "lsp-execute-code-action")
    "mr" '(lsp-rename :which-ket "lsp-rename")
    ;;etc
    "i" '(:ignore t :which-key "/etc")
    "ii" '(swiper :which-key "swiper")
    ))

(defun rune/evil-hook ()
  (dolist (mode '(custom-mode
		  eshell-mode
		  shell-mode
		  git-rebase-mode
		  erc-mode
		  circe-server-mode
		  circe-chat-mode
		  circe-query-mode
		  sauron-mode
		  term-mode))
    (add-to-list 'evil-emacs-state-modes mode)))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  (evil-mode 1)
  :hook (evil-mode . rune/evil-hook)
  :config
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

;;(quelpa '(term-cursor :repo "h0d/term-cursor.el" :fetcher github))

(use-package term-cursor
  :init (global-term-cursor-mode))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))
;;Smartparens
(use-package smartparens
  :init (smartparens-mode 1)
  :config
  (smartparens-global-mode t))

;;Git shit
(use-package projectile
  :diminish projectile-mode
  :config
  (projectile-mode 1)
  :custom
  (projectile-completion-system 'ivy)
  :init
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit)

(defun rune/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

(defun rune/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
;; Set faces for heading levels
(dolist (face '((org-level-1 . 1.2)
                (org-level-2 . 1.1)
                (org-level-3 . 1.05)
                (org-level-4 . 1.0)
                (org-level-5 . 1.1)
                (org-level-6 . 1.1)
                (org-level-7 . 1.1)
                (org-level-8 . 1.1)))
  (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))

;; Ensure that anything that should be fixed-pitch in Org files appears that way
(set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
;;(set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))
)

(use-package org
  :hook (org-mode . rune/org-mode-setup)
  :config
  ;; (setq org-ellipsis " ▾")
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  
  (setq org-agenda-files
	'("~/.emacs.d/org_files/tasks.org"))
  (rune/org-font-setup))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun rune/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . rune/org-mode-visual-fill))

(defun rune/lsp-mode-setup ()
  (setq lsp-headerline-breadcrimb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook (lsp-mode . rune/lsp-mode-setup)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-enable-which-key-integration t))

(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :custom
  (company-minimum-prefix-length 1)
  (comapny-idle-delay 0.0))

(use-package flycheck)

(defun rune/rustic-keys ()
    )

(use-package rustic
  :ensure
  :hook
  (rustic-mode . lsp-deferred)
  (rustic-mode . rune/rustic-keys)
  :config
  (setq rustic-indent-level 4))


(defun on-after-init ()
  (unless (display-graphic-p (selected-frame))
    (set-face-background 'default "unspecified-bg" (selected-frame))))

(add-hook 'window-setup-hook 'on-after-init)

;;Lets use this later
;;(use-package forge)
;;Custom file    
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)
